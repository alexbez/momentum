<?php
/**
 * Created by PhpStorm.
 * User: Alex B
 * Date: 2018-08-12
 * Time: 8:15 PM
 */


//Get data from https://swapi.co/documentation
$people = json_decode(file_get_contents('https://swapi.co/api/people/?format=json'),true)['results'];
$planets = json_decode(file_get_contents('https://swapi.co/api/planets/?format=json'),true)['results'];
$starships = json_decode(file_get_contents('https://swapi.co/api/starships/?format=json'),true)['results'];

?>

<!DOCTYPE html>
<html>
    <head>
    <!--Style-->
    <link rel="stylesheet" type="text/css" href="style.css" />
    <!--Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Import vue.js -->
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    </head>

    <body>


    <!-- The header (logo/title)-->
    <div class="header">
        <img src="pictures/logo.png" class="headerPic" height="42" width="250">
        <h1 align="center">Momentum Developer Aptitude Test</h1>
        <h5 align="center">By: Alex Bezouglov</h5>
    </div>

    <!-- the maint container window -->
    <div class="mainContainer">
        <!-- The select/input div -->
        <div class="dataContainer">

            <!--People list -->
            <div id="people">
                <label>People: </label>
                <select v-model="selected">
                    <option v-for="option in options" v-bind:value="option.value">
                        {{ option.text }}
                    </option>
                </select>
            </div>

            <!--Planet list -->
            <div id="planet">
                <label>Planets: </label>
                <select v-model="selected">
                    <option v-for="option in options" v-bind:value="option.value">
                        {{ option.text }}
                    </option>
                </select>
            </div>

            <!--Starship list -->
            <div id="starship">
                <label>Starships: </label>
                <select v-model="selected">
                    <option v-for="option in options" v-bind:value="option.value">
                        {{ option.text }}
                    </option>
                </select>
            </div>

            <!--Search Person -->
            <div id="Person_search">
                <input v-model="searchText">
                <div v-if="items">
                    <ul>
                        <li v-for="person in personList">{{ person }} </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    </body>
</html>

<script>
    
    new Vue({
        el: '#people',
        data: {
            selected: '',
            options: [
                <?php foreach($people as $person){?>
                { text: '<?=$person['name'];?>', value: '<?=$person['name'];?>'},
                <?php } ?>
            ]
        }
    })

    new Vue({
        el: '#planet',
        data: {
            selected: '',
            options: [
                <?php foreach($planets as $planet){?>
                { text: '<?=$planet['name'];?>', value: '<?=$planet['name'];?>'},
                <?php } ?>
            ]
        }
    })

    new Vue({
        el: '#starship',
        data: {
            selected: '',
            options: [
                <?php foreach($starships as $starship){?>
                { text: '<?=$starship['name'];?>', value: '<?=$starship['name'];?>'},
                <?php } ?>
            ]
        }
    })

    new Vue({

        el: '#Person_search',

        data: {
            items: [
                <?php foreach($people as $person){?>
                "<?=$person['name'];?>",
                <?php } ?>
            ],
            searchText: ''
        },

        computed : {
            personList : function(){
                var self = this;
                if( this.searchText == ''){
                    return '';
                }
                return this.items.filter(function(item){
                    return item.indexOf(self.searchText) >= 0;
                });
            }
        }
    });

</script>